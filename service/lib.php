<?php

function dd( $var, $msg = null )
{
    echo "<pre>";
    var_dump( $var );
    echo "</pre>";
    die( $msg );
}