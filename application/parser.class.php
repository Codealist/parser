<?php
namespace application;

class Parser
{
    private $to_parse;
    private $dir_name;
    private $project_directory;


    public function __construct( $to_parse, $dir_name )
    {
        $this->to_parse = $to_parse;
        $this->dir_name = $dir_name;
    }

    public function createProjectDirectory()
    {
        mkdir(PROJ_ROOT."/".$this->dir_name, 0755);
        $this->project_directory = PROJ_ROOT."/".$this->dir_name;
    }

    public function parseJsFiles()
    {
        //get page source
        ob_start();
        include($this->to_parse);
        $reg_input = ob_get_clean();
        $matches = array();
        //$mask = "/\<(script).*(src.*\=.*\.(js).*)\>/";
        $mask = "/\<(script).*(src.*\=.*\.(js).*)\>/";
        preg_match_all($mask, $reg_input, $matches);

        //experimental
//        foreach($matches as &$item){
//            if(is_array($item)){
//                foreach($item as $elem){
//                    htmlentities($elem);
//                }
//            } else {
//                htmlentities($item);
//            }
//        }
        return $matches;
    }


}